# 必要条件
Pythonがインストールされていること。
Pythonのバージョンは3.8以上であること。

# 推奨インストール手順
## acs-deeplダウンロード
以下のサイトからacs-deepl.zipをダウンロードする。
https://gitlab.com/HirMtsd/acs-deepl

## Python仮想環境の設定
任意のフォルダ(例：C:\Dev\asc-deepl)を作成する。
以下のコマンド実行し、DeepL用のPython仮想環境を作成する。
set ASC-DEEPL-HOME=C:\Dev\asc-deepl
pushd %ASC-DEEPL-HOME%
python -m venv %ASC-DEEPL-HOME%
Scripts\activate.bat
python -m pip install --upgrade deepl
deactivate
popd

## DeepLアカウントキーの設定
ダウンロードしたasc-deepl.zipをC:\Dev\asc-deeplに展開する。
DeepLKey.pyファイルにDeepLアカウントキーを記載する。

# 動作手順
以下のバッチファイル(Asc-DeepL.bat)を作成しておくと便利
set ASC-DEEPL-HOME=C:\Dev\asc-deepl
pushd %ASC-DEEPL-HOME%
call Scripts\activate.bat & python DL_GUI.py
deactivate
popd

# TODO
・出力をTXTに限定？
・空白を含むファイルに対応
・動作ログを出力
・動作完了時に使用量を表示
