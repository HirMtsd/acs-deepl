#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" DeepL APIを使用してテキストファイルの翻訳を行うサンプルプログラム。 """
# DeepL APIを使用してファイルの翻訳を行う場合、[ファイルアップロード][状態監視][結果ダウンロード]の
# 3つのステップを実行しなければならない。この作業を一度の操作で済ませるためのプログラム。

__author__    = "@HirMtsd"
__copyright__ = "Copyright 2024, @HirMtsd"
__date__      = "2024/01/10"
__email__     = "hirmtsd@gmail.com"
__version__   = "0.1" # see CHANGELOG.md
__status__    = "Development"

# DeepL APIについては以下を参照
# https://www.deepl.com/
# https://github.com/DeepLcom/deepl-python

# DeepL APIを使用するための「認証キー」を事前に取得しておく必要がある。
# 取得したキーをDeepLKey.pyファイルに以下の形式で格納しておく
# auth_key = '(認証キー)'

# venvで仮想環境を作成し、以下のコマンドでインストールすることを推奨。
# python -m pip install --upgrade deepl

# Windows環境で、venvを使用している場合、shebangのせいで、
# 「ModuleNotFoundError: No module named 'Crypto'」
# が表示される場合は、pyランチャを使用しないで「python pyex_deepl.py」と起動する。
# https://www.python.org/dev/peps/pep-0397/

from DeepLKey import auth_key
import argparse
import sys
import os
import deepl

# 変換処理の事前準備(引数処理)
def main():
  parser = argparse.ArgumentParser(description='DeepL APIを使用してファイルの翻訳を行うサンプルプログラム')
  parser.add_argument('in_file', help='翻訳元ファイル')
  parser.add_argument('-o', '--out_file', help='出力ファイル')
  parser.add_argument('-d', '--destination', default='JA', choices=['BG', 'CS', 'DA', 'DE', 'EL', 'EN', 'ES', 'ET', 'FI', 'FR', 'HU', 'ID', 'IT', 'JA', 'KO', 'LT', 'LV', 'NB', 'NL', 'PL', 'PT', 'RO', 'RU', 'SK', 'SL', 'SV', 'TR', 'UK', 'ZH'], help='翻訳先言語(指定しない場合は日本語)')
  parser.add_argument('-s', '--source', help='翻訳元言語')
  args = parser.parse_args()

  print(auth_key)
  print(args.in_file)
  print(args.out_file)
  print(args.destination)
  print(args.source)

  # 引数の編集
  input_path = os.path.normpath(args.in_file)
  if not args.out_file:
    output_path = ''
  else:
    output_path = os.path.normpath(args.out_file)
  # input_path = os.path.join('C:' + os.sep, 'Dev', 'DeepL', 'in_en.txt')
  # output_path = os.path.join('C:' + os.sep, 'Dev', 'DeepL', 'out_ja.txt')
  dst = args.destination
  src = args.source  

  print(input_path)
  print(output_path)
  transrate(auth_key, input_path, output_path, dst)

# DeepL APIを使用した変換処理
# TODO: 2024/01/10 source_langを指定可能にしたい
# TODO: 2024/01/10 formalityを指定可能にしたい
# TODO: 2024/01/10 glossaryを指定可能にしたい
def transrate(auth_key, input_path, output_path, dst):
  try:
    translator = deepl.Translator(auth_key)
    # Using translate_document_from_filepath() with file paths 
    translator.translate_document_from_filepath(
      input_path,
      output_path,
      target_lang=dst,
      formality="more"
    )

    # TODO: 2024/01/10 デフォルト出力先をsys.stdoutにしたい。
    # # Alternatively you can use translate_document() with file IO objects
    # with open(input_path, "rb") as in_file, open(output_path, "wb") as out_file:
    #     translator.translate_document(
    #         in_file,
    #         out_file,
    #         target_lang="DE",
    #         formality="more"
    #     )

  # TODO: 2024/01/10 エラー出力先のデフォルトを標準エラー出力にしたい
  # TODO: 2024/01/10 エラー出力先を指定可能にしたい
  except deepl.DocumentTranslationException as error:
    # If an error occurs during document translation after the document was
    # already uploaded, a DocumentTranslationException is raised. The
    # document_handle property contains the document handle that may be used to
    # later retrieve the document from the server, or contact DeepL support.
    doc_id = error.document_handle.id
    doc_key = error.document_handle.key
    print(f"Error after uploading ${error}, id: ${doc_id} key: ${doc_key}")
  except deepl.DeepLException as error:
    # Errors during upload raise a DeepLException
    print(error)

def chk_usage():
  try:
    translator = deepl.Translator(auth_key)
    # Check account usage
    usage = translator.get_usage()
    if usage.character.limit_exceeded:
        print("Character limit exceeded.")
    else:
        print(f"Character usage: {usage.character}")
  except deepl.DeepLException as error:
    # Errors during upload raise a DeepLException
    print(error)

if __name__ == "__main__":
  sys.exit(main())
