#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" DL.pyのGUIラッププログラム。 """

__author__    = "@HirMtsd"
__copyright__ = "Copyright 2024, @HirMtsd"
__date__      = "2024/01/10"
__email__     = "hirmtsd@gmail.com"
__version__   = "0.1" # see CHANGELOG.md
__status__    = "Development"

# DL.pyファイルを起動するためのGUIラッププログラム
# 詳細は、DL.pyを確認。
# 出力は日本語に固定。

import tkinter
from tkinter import ttk
from tkinter import filedialog
import subprocess
import os
import re

def win():
  # btn1[参照]の処理
  def i_file():
    fTyp = [("", "*")]
    iDir = os.path.abspath(os.path.dirname(__file__))
    filepath = filedialog.askopenfilename(filetype = fTyp, initialdir = iDir)
    txt_1.set(filepath)
    
    # 変換候補の自動入力
    fh = re.findall(r'(.+)\..+',filepath)
    ft = re.findall(r'.+(\..+)',filepath)
    t2cand = fh[0] + '_jp_' + ft[0]
    if t2cand == filepath:
      t2cand = filepath + '_jp'
    
    txt_2.set(t2cand)
  
  # btn2[参照]の処理
  def o_file():
    fTyp = [("", "*")]
    iDir = os.path.abspath(os.path.dirname(__file__))
    filepath = filedialog.askopenfilename(filetype = fTyp, initialdir = iDir)
    txt_2.set(filepath)
  
  # btn3[翻訳]の処理
  def trans():
    infile = txt_1.get()
    outfile = txt_2.get()
    if infile == outfile:
      lbl_3.set('入力ファイルと出力ファイルが同じです')
      return
    if outfile == "":
      lbl_3.set('出力ファイルを指定する必要があります')
      return
    lbl_3.set('翻訳中')
    r = callDL(infile, outfile)
    lbl_3.set('翻訳出力完了' + r)
  
  # btn4[クリア]の処理
  def clear():
    txt_1.set('')
    txt_2.set('')
    lbl_3.set('')
  
  # tkinterでウィンドウを表示する
  wr = tkinter.Tk()
  wr.title("DeepL APIを使用したファイル単位の翻訳")
  wr.resizable(False, False)
  
  # テキスト準備
  txt_1 = tkinter.StringVar()
  txt_2 = tkinter.StringVar()
  lbl_3 = tkinter.StringVar()
  
  lbl1 = ttk.Label(wr, text = "翻訳元(入力)ファイル")
  lbl2 = ttk.Label(wr, text = "翻訳先(出力)ファイル")
  lbl3 = ttk.Label(wr, textvariable = lbl_3)
  txt1 = ttk.Entry(wr, width = 50, textvariable = txt_1)
  txt2 = ttk.Entry(wr, width = 50, textvariable = txt_2)
  btn1 = ttk.Button(wr, text = "参照", command = lambda:i_file())
  btn2 = ttk.Button(wr, text = "参照", command = lambda:o_file())
  btn3 = ttk.Button(wr, text = "翻訳", command = lambda:trans())
  btn4 = ttk.Button(wr, text = "クリア", command = lambda:clear())
  btn5 = ttk.Button(wr, text = "終了", command=quit)
  
  # レイアウト
  lbl1.grid(row=0, column=0)
  lbl2.grid(row=1, column=0)
  txt1.grid(row=0, column=1)
  txt2.grid(row=1, column=1)
  btn1.grid(row=0, column=2)
  btn2.grid(row=1, column=2)
  lbl3.grid(row=2, column=1)
  btn3.grid(row=3, column=0)
  btn4.grid(row=3, column=1)
  btn5.grid(row=3, column=2)
  
  # Windowの表示
  wr.mainloop()

def callDL(infile, outfile):
  # サブプロセスとしてDL.pyを呼び出す
  cmd = 'python DL.py ' + infile + ' -o ' + outfile
  print(cmd)
  
  proc = subprocess.run(cmd, shell=True)
  
  return proc

win()
